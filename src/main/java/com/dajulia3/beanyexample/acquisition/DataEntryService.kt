package com.dajulia3.beanyexample.acquisition

import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import java.util.*

@Service
class DataEntryService(val mongoTemplate: ReactiveMongoTemplate) {
    fun createEntry(entryUnpersisted: UnpersistedDataEntry): DataEntry {
        val dataEntry = DataEntry(deviceId = entryUnpersisted.deviceId, rows = entryUnpersisted.rows)
        return mongoTemplate.insert(dataEntry).block()!!
    }

    fun findEntry(id: String): DataEntry? {
        return mongoTemplate.findById(id, DataEntry::class.java).block()
    }

    fun findAll(): Flux<DataEntry> {
        return mongoTemplate.findAll(DataEntry::class.java)
    }

    data class DataEntryCreationResult(val id: String)
}

data class DataEntry(val deviceId: String, val rows: List<List<String>>, val id: String = UUID.randomUUID().toString()) {
    companion object {
        fun fromUnpersisted(unpersisted: UnpersistedDataEntry): DataEntry = DataEntry(deviceId = unpersisted.deviceId, rows = unpersisted.rows)
    }
}

data class UnpersistedDataEntry(val deviceId: String, val rows: List<List<String>>)

