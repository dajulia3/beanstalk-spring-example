package com.dajulia3.beanyexample.acquisition

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import java.net.URI

@RestController
@RequestMapping("/data-entries")
class DataEntriesController @Autowired constructor(private val service: DataEntryService) {
    @PostMapping
    fun create(@RequestBody entryUnpersisted: UnpersistedDataEntry): ResponseEntity<CreationSuccessResponse> {
        val result = service.createEntry(entryUnpersisted)
        val link = "/data-entries/" + result.id
        return ResponseEntity.created(URI.create(link)).body(
                CreationSuccessResponse(CreationResponseLinks(link))
        )
    }

    @GetMapping("/{id}")
    fun view(@PathVariable("id") id: String): ResponseEntity<DataEntry?> {
        service.findEntry(id = id)?.let {
            return ResponseEntity.ok(it)
        }
        return ResponseEntity.notFound().build()
    }

    @GetMapping("")
    fun index(): ResponseEntity<Flux<DataEntry>> {
        val entries = service.findAll()
        return ResponseEntity.ok(entries)
    }

    data class CreationSuccessResponse(val _links: CreationResponseLinks)

    data class CreationResponseLinks constructor(val entry: String)
}
