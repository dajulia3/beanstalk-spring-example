package com.dajulia3.beanyexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeanyExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeanyExampleApplication.class, args);
	}

}
