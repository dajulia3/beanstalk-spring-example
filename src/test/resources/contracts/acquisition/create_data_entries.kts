package acquisition

import org.springframework.cloud.contract.spec.ContractDsl.Companion.contract
import java.util.*

contract {
    description = "should respond with created and give the link to the data entry"
    request {
        url = url("/data-entries")
        method = POST
        headers {
            contentType = APPLICATION_JSON
        }
        body = body("""
                {
                    "deviceId": "abc-123",
                    "rows":
                        [
                                ["row1-entry1", "row1-entry2"],
                                ["row2-entry1", "row2-entry2"]
                        ]
                }
            """)
    }

    response {
        status = CREATED

        headers {
            contentType = APPLICATION_JSON
            location = "/data-entries/xxx-yyy-zzz"
        }
        body(""" {
            "_links": {
            "entry": "/data-entries/xxx-yyy-zzz"
            }
        }""")
    }
}
