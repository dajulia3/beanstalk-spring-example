package com.dajulia3.beanyexample;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
class BeanyExampleApplicationTests {

	@Test
	void contextLoads() {
	}

}
