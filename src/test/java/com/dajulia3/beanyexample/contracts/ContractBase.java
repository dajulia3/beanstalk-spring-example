package com.dajulia3.beanyexample.contracts;

import com.dajulia3.beanyexample.acquisition.DataEntriesController;
import com.dajulia3.beanyexample.acquisition.DataEntry;
import com.dajulia3.beanyexample.acquisition.DataEntryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static io.restassured.config.EncoderConfig.encoderConfig;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.config;
import static org.mockito.ArgumentMatchers.notNull;

@ActiveProfiles("test")
@WebMvcTest(DataEntriesController.class)
public abstract class ContractBase {
    @Autowired
    WebApplicationContext webApplicationContext;

    @MockBean
    DataEntryService service;

    @BeforeEach
    public void setup() {
        //A little weird to just return random data here... maybe we can refine this behavior.
        Mockito.when(service.createEntry(notNull()))
                .thenReturn(new DataEntry("abc-123", Arrays.asList(Arrays.asList("1")), "xxx-yyy-zzz"));

        RestAssuredMockMvc.config = config().encoderConfig(encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));
        RestAssuredMockMvc.webAppContextSetup(webApplicationContext);
        RestAssuredMockMvc.enableLoggingOfRequestAndResponseIfValidationFails();
    }

}
