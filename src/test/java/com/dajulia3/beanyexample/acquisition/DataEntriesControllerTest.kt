package com.dajulia3.beanyexample.acquisition

import com.fasterxml.jackson.databind.ObjectMapper
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.json.JsonTest
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import reactor.core.publisher.Flux

@JsonTest
internal class DataEntriesControllerTest {
    private lateinit var service: DataEntryService

    @Autowired
    lateinit var mapper: ObjectMapper

    lateinit var mockMvc: MockMvc

    @BeforeEach()
    fun setup() {
        service = mockk()
        val controller = DataEntriesController(service)
        mockMvc = standaloneSetupWithJson(controller);
    }

    private fun standaloneSetupWithJson(controller: Any): MockMvc {
        val converter = MappingJackson2HttpMessageConverter()
        converter.objectMapper = mapper
        return MockMvcBuilders.standaloneSetup(controller).setMessageConverters(converter).build()
    }

    @Nested
    inner class creating() {

        @Test
        fun `it creates an entry`() {
            val unpersisted = UnpersistedDataEntry(
                    deviceId = "abc-123",
                    rows = listOf(listOf("1-1", "1-2"), listOf("2-1", "2-2")))

            every { service.createEntry(any()) } returns DataEntry.fromUnpersisted(unpersisted)

            mockMvc.post("/data-entries") {
                accept = MediaType.APPLICATION_JSON
                contentType = MediaType.APPLICATION_JSON
                content = """{
                        "deviceId":"abc-123",
                        "rows":[["1-1","1-2"],["2-1","2-2"]]
                    }"""
            }.andExpect { status { isCreated } }

            verify {
                service.createEntry(unpersisted)
            }
        }
    }


    @Nested
    inner class finding() {
        @Test
        fun `returns then entity if found`() {
            val dataEntry = DataEntry(id = "123456", deviceId = "abc-123", rows = listOf(listOf("A", "B")))
            every { service.findEntry(any()) } returns dataEntry

            mockMvc.get("/data-entries/123456") {
                accept = MediaType.APPLICATION_JSON
                contentType = MediaType.APPLICATION_JSON
            }.andExpect { status { isOk } }

            verify {
                service.findEntry("123456")
            }
        }

        @Test
        fun `returns an error if not found`() {
            every { service.findEntry(any()) } returns null

            mockMvc.get("/data-entries/123456") {
                accept = MediaType.APPLICATION_JSON
                contentType = MediaType.APPLICATION_JSON
            }.andExpect { status { isNotFound } }

        }
    }

    @Nested
    inner class index() {
        @Test
        fun `returns all entries`() {
            val dataEntryList = Flux.just(
                    DataEntry(id = "123456", deviceId = "abc-123", rows = listOf(listOf("A", "B"))),
                    DataEntry(id = "789011", deviceId = "xyz-999", rows = listOf(listOf("C", "D")))
            )
            every { service.findAll() } returns dataEntryList

            mockMvc.get("/data-entries") {
                accept = MediaType.APPLICATION_JSON
                contentType = MediaType.APPLICATION_JSON
            }.andExpect { status { isOk } }

        }
    }

}
