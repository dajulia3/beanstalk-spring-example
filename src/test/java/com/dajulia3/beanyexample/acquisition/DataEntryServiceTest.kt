package com.dajulia3.beanyexample.acquisition

import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension
import reactor.test.StepVerifier

@DataMongoTest(properties = ["spring.data.mongodb.uri=mongodb://root:example@127.0.0.1:27017/admin"])
@ExtendWith(SpringExtension::class)
internal class DataEntryServiceTest {

    @Autowired
    lateinit var mongoTemplate: ReactiveMongoTemplate

    lateinit var service: DataEntryService

    @BeforeEach
    fun setup() {
        mongoTemplate.dropCollection(DataEntry::class.java).block()
        service = DataEntryService(mongoTemplate)
    }


    @Nested
    inner class Save() {
        @Test
        fun `saves the data entry`() {
            val entry = UnpersistedDataEntry(deviceId = "abc12345", rows = listOf(
                    listOf("1-1", "1-2"),
                    listOf("2-1", "2-2")
            ))
            val creationResult = service.createEntry(entry)

            val lookupResult = service.findEntry(creationResult.id)

            assertThat(lookupResult).isEqualTo(DataEntry(
                    id = creationResult.id,
                    deviceId = "abc12345",
                    rows = listOf(
                            listOf("1-1", "1-2"),
                            listOf("2-1", "2-2")
                    )))
        }
    }

    @Nested
    inner class findAll() {
        @Test
        fun `returns all the entries`() {
            val unsavedEntry1 = UnpersistedDataEntry(deviceId = "abc12345", rows = listOf(
                    listOf("1-1", "1-2"),
                    listOf("2-1", "2-2")
            ))
            val unsavedEntry2 = UnpersistedDataEntry(deviceId = "def98765", rows = listOf(
                    listOf("1-1", "1-2"),
                    listOf("2-1", "2-2")
            ))
            val entry1 = service.createEntry(unsavedEntry1)
            val entry2 = service.createEntry(unsavedEntry2)

            val lookupResult = service.findAll().log()

            StepVerifier.create(lookupResult)
                    .expectNext(entry1)
                    .expectNext(entry2)
                    .verifyComplete()
        }
    }

}
